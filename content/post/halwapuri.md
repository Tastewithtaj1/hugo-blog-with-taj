---
title: "Halwa Puri Chana Recipe"
description: 
date: 2022-02-18T15:49:54+05:00
image: https://res.cloudinary.com/taste-with-taj/image/upload/v1644075483/Blog-Assets%20TwT/Halwa%20Puri%20Chana%20Recipe/Head_Image_Website_tg5nap.jpg
categories:
    - Break Fast

tags:
    - halwa puri recipe
    - halwa puri chana recipe 
    - halwa puri aloo chana recipe
math: 
license: 
hidden: false
comments: true
draft: false
---

{{< youtube ROWGZzi9qZM >}}


## Description

Tandoori Chai Recipe

Today Taste with Taj is going to make Tandoori Chai Recipe which quite easy. Just follow step to make Tandoori Chai Recipe

Ingredients
Chocolate   2 Small Pieces
Chae ki Pati (Tea)  1 1/2 Tsp
Sugar    As Requird
Cardamom   1
Milk    1/2 Cup
Water    1/2 Cup
Method
⦁ Take small pot made by mud, Put that pot on stove on small heat hai take it to 25 min until it become Red hot.
⦁ One the other hand Make tea add Chocolate,Chae ki Pati (Tea),Sugar,Cardamom,Milk,Water in sos pan and take it on medium heat .
⦁ When its nearly to boil mix with spoon and boil completely.
⦁ Strain tea into a small glass or jug.
⦁ When mud pot hot enough put it into an bowl and instently add hot tea into pot and you will see the magic.
⦁ This tea is really delicious and you will feel fun we beat hahah :)
⦁ Allhumdullillah.


## Gallery pictures

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1644075831/Blog-Assets%20TwT/Halwa%20Puri%20Chana%20Recipe/Gallery_Image_1_nvbn2a.jpg)  ![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1644075832/Blog-Assets%20TwT/Halwa%20Puri%20Chana%20Recipe/Gallery_Image_4_d8vtax.jpg)


![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1644075831/Blog-Assets%20TwT/Halwa%20Puri%20Chana%20Recipe/Gallery_Image_2_ss44bh.jpg)  ![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1644075831/Blog-Assets%20TwT/Halwa%20Puri%20Chana%20Recipe/Gallery_Image_3_fu6v9k.jpg)

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1644075831/Blog-Assets%20TwT/Halwa%20Puri%20Chana%20Recipe/Gallery_Image_5_la2p7o.jpg)

