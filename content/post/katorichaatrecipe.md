---
title: "Katori Chaat Recipe"
description: 
date: 2022-02-18T15:54:10+05:00
image: https://res.cloudinary.com/taste-with-taj/image/upload/v1644130829/Blog-Assets%20TwT/Katori%20Chaat%20Recipe/Header_Image_mhnne2.jpg
categories:
    - Break Fast

tags:
    - Papri chaat
    - Papri chaat recipe
    - Dahi papdi chaat recipe
math: 
license: 
hidden: false
comments: true
draft: false

---

{{< youtube nUbjCBWpG1M >}}

## Description
Finally we are here with Katori Chaat Recipe. Again very famous food street but if you make Katori Chaat Recipe in home that is best and hygiene. I hope you will enjoy all steps. 

Ingredients for  Katori Chaat Recipe

FOR CHANA CHAAT

Boiled Channay(CHICKPEA) 1 Cup

Boiled Aloo (POTATOES)  1 Cup

Piyaz (ONION)   1 Medium Size

Tamatar (TOMATO)  1 Medium Size

Dhaniya(CORIANDER)  1 Tbsp

Podina (MINT)   1 Tbsp

Namak (SALT)   As Required

CHAT MASALA   1 Tsp

IMLI CHATNI   2 Tbsp

FOR GREEN CHATNI

Dhaniya(CORIANDER)  1 Tbsp

Podina (MINT)   1 Tbsp

Hari Mirch (GREEN CHILLI)  1

FOR PAPRI

ROLL PATI   2 Per Papri

FOR DAHI PAPRI

Dahi (Yogurt)   2 Tbsp


Method of Home Made Channa Papri and Dahi Papri Ramazan Special

Take 2 Roll Pati to make Papri set then into star shape.

-Put into oil for deep fry on medium heat and add a large cup spoon in them middle,take this spoon into bottom of pain.

-When Papri comes hard and make a flower shape removr that spoon and fry full papri take warm oil and add that oil into papri bowl to cook inside off the papri bowl.

-Add Dhaniya(Coriander),Podina (Mint) ,Hari Mirch (Green chilli) and blend it.

-Take a Large Bowl add Boiled Channay(chickpea),Boiled Aloo(Potatoes),Piyaz (onion),Tamatar (tomato),Dhaniya(Coriander),Podina (Mint),Namak (Salt),Chat Masala,Imli Chatni, and mix them very well.

FOR CHANA PAPRI

-Put Chana Chaat into papri and add 1 tsp of green chatni and 1 tsp of imli chatni.

FOR DAHI PAPRI

-Put Chana Chaat into papri and add 1 tsp of green chatni and 1 tsp of imli chatni and 2 Tbsp Dahi (Yogurt).


## Gallery Pictures


![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1644130829/Blog-Assets%20TwT/Katori%20Chaat%20Recipe/Gallery_Image_2_xgfdlh.jpg)  ![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1644130829/Blog-Assets%20TwT/Katori%20Chaat%20Recipe/Gallery_Image_1_t82oiw.jpg)

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1644130829/Blog-Assets%20TwT/Katori%20Chaat%20Recipe/Gallery_Image_1_t82oiw.jpg)  ![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1644130829/Blog-Assets%20TwT/Katori%20Chaat%20Recipe/Gallery_Image_3_rwuvgv.jpg)