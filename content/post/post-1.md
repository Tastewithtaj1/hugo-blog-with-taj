---
title: "Tandoori Chai Recipe"
description: 
date: 2022-02-16T17:31:07+05:00
image: https://res.cloudinary.com/taste-with-taj/image/upload/v1644068704/Blog-Assets%20TwT/Tandoori%20Chai%20Recipe/Header_Image_tvtk1v.jpg
categories:
    - Drinks

tags:
    - Tandoori Chai Recipe
    - Tandoori chai recipe at home
    - Recipe of tandoori chai
math: 
license: 
hidden: false
comments: true
draft: false
---

{{< youtube bKA2Osk6GGo >}}


Tandoori Chai Recipe

Today Taste with Taj is going to make Tandoori Chai Recipe which quite easy. Just follow step to make Tandoori Chai Recipe

Ingredients
Chocolate   2 Small Pieces
Chae ki Pati (Tea)  1 1/2 Tsp
Sugar    As Requird
Cardamom   1
Milk    1/2 Cup
Water    1/2 Cup
Method
⦁ Take small pot made by mud, Put that pot on stove on small heat hai take it to 25 min until it become Red hot.
⦁ One the other hand Make tea add Chocolate,Chae ki Pati (Tea),Sugar,Cardamom,Milk,Water in sos pan and take it on medium heat .
⦁ When its nearly to boil mix with spoon and boil completely.
⦁ Strain tea into a small glass or jug.
⦁ When mud pot hot enough put it into an bowl and instently add hot tea into pot and you will see the magic.
⦁ This tea is really delicious and you will feel fun we beat hahah :)
⦁ Allhumdullillah.


## Gallery pictures

![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1644068768/Blog-Assets%20TwT/Tandoori%20Chai%20Recipe/Gallery_image_1_u6mxod.jpg)  ![Photo by Explore with Taj](https://res.cloudinary.com/taste-with-taj/image/upload/v1644068902/Blog-Assets%20TwT/Tandoori%20Chai%20Recipe/Gallery_image_2_yn95us.jpg)
