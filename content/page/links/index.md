---
title: Links
links:
  - title: Instagram
    website: https://www.instagram.com/taste.with.taj/
    image: https://res.cloudinary.com/taste-with-taj/image/upload/v1643912938/Common-assets%20TwT/Insta_Logo_k0rrjh.jpg
  - title: Facebook
    website: https://www.facebook.com/tastewithtaj
    image: https://res.cloudinary.com/taste-with-taj/image/upload/v1643913228/Common-assets%20TwT/face_book_rlyg15.jpg
  - title: Pinterest
    website: https://www.pinterest.com/tastewitht/_saved/
    image: https://res.cloudinary.com/taste-with-taj/image/upload/v1643913756/Common-assets%20TwT/Pintrest_twt_rbvu6m.jpg
  - title: Youtube
    website: https://www.youtube.com/watch?v=bKA2Osk6GGo
    image: https://res.cloudinary.com/taste-with-taj/image/upload/v1646564495/Common-assets%20TwT/YT_logo_for_blog_oefkhm.jpg
menu:
    main: 
        weight: -50
        params:
            icon: link

comments: false
---